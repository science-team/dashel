Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: dashel
Source: https://github.com/aseba-community/dashel/archive/1.3.3.tar.gz

Files: *
Copyright: 2007-2017 Stéphane Magnenat (http://stephane.magnenat.net)
           2007-2017 Mobots group (http://mobots.epfl.ch)
           2007-2017 EPFL (http://www.epfl.ch/)
           2007-2017 Game Technology Center (http://www.gtc.inf.ethz.ch/)
           2007-2017 Game ETH Zurich (https://www.ethz.ch/en.html)
           2007-2017 Sebastian Gerlach, [Kenzan Technologies] (http://www.kenzantech.com)
           2007-2017 Antoine Beyeler (http://www.ab-ware.com)
           2007-2017 Laboratory of Intelligent Systems (http://lis.epfl.ch)
           2007-2017 David James Sherman (http://www.labri.fr/perso/david/Site/David_James_Sherman.html)
           2007-2017 Inria (http://inria.fr)
           2007-2017 Yves Piguet (http://nyctergatis.com/)
License: BSD-3-Clause

Files: debian/*
Copyright: 2018 Georges Khaznadar <georgesk@debian.org>
License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: BSD-3-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 * Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
 .
 * Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.
 .
 * Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 